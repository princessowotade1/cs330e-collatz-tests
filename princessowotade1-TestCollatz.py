#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # ----
    # read
    # ----
    
    #default
    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    #Same string
    def test_read_2(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)

    #Reverse
    def test_read_3(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    #MoreThan2
    def test_read_4(self):
        s = "1 10 65\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)
	
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

#Added tests follows
     
    def test_eval_5(self):
        v = collatz_eval(223, 2599)
        self.assertEqual(v, 209)

    def test_eval_6(self):
        v = collatz_eval(25, 25)
        self.assertEqual(v, 24)

    def test_eval_7(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)
    
    def test_eval_8(self):
        v = collatz_eval(724852, 724070)
        self.assertEqual(v, 362)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
#Added tests follows

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2, 30, 80)
        self.assertEqual(w.getvalue(), "2 30 80\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 1, 19)
        self.assertEqual(w.getvalue(), "1 1 19\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 40, 50, 60)
        self.assertEqual(w.getvalue(), "40 50 60\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

#Added tests follows

    def test_solve_2(self):
        r = StringIO("2 10\n20 45\n50 89\n1 125\n23 92\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "2 10 20\n20 45 112\n50 89 116\n1 125 119\n23 92 116\n")

    def test_solve_3(self):
        r = StringIO("1 8\n23 45\n888 989\n1000 2021\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 8 17\n23 45 112\n888 989 174\n1000 2021 182\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
